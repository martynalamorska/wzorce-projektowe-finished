package command;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Executor {

    private List<Command> commandList;

    public List<Command> getCommandList() {
        return commandList;
    }

    public Executor() {
        this.commandList = new LinkedList<>();
    }

    public void executeCommand(Command command) throws IOException {
        commandList.add(command);
        command.execute();

    }

}
