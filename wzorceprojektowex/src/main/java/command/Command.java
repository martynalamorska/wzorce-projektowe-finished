package command;

import java.io.IOException;

public abstract class Command {

    private Room room;

    public Room getRoom() {
        return room;
    }

    public Command(Room room) {
        this.room = room;
    }

    public abstract void execute() throws IOException;
}
