package command;

import java.io.IOException;

public class LightsOffCommand extends Command {

    public LightsOffCommand(Room room) {
        super(room);
    }

    @Override
    public void execute() throws IOException {
        getRoom().setLights(false);
    }
}
