package command;

import singleton.Logger;

import java.io.IOException;

public class Room {

    String roomName;
    boolean lights;
    String music;

    public Room(String roomName) {
        this.roomName = roomName;
        lights = false;
        music = "";
    }

    @Override
    public String toString() {
        return "Current room: " + '\n' +
                "Name: " + roomName + '\n' +
                "Lights: " + lights + '\n' +
                "Music: " + music;
    }

//    @Override
//    public String toString() {
//        return "We're in the room " + roomName +
//                ", the lights are " + lights +
//                " and the music playing is " + music + '.';
//    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) throws IOException {
        this.roomName = roomName;
        Logger.getLogger().log("The name of the room is " + roomName);
    }

    public boolean isLights() {
        return lights;
    }

    public void setLights(boolean lights) throws IOException {
        this.lights = lights;
        Logger.getLogger().log("The lights are " + lights);

    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) throws IOException {
        this.music = music;
        Logger.getLogger().log("The track playing is " + music);

    }


}
