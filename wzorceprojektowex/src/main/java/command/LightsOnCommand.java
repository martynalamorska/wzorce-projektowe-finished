package command;

import java.io.IOException;

public class LightsOnCommand extends Command{

    private boolean previous;

    public LightsOnCommand(Room room) {
        super(room);
    }

    @Override
    public void execute() throws IOException {
        previous = getRoom().isLights();
        getRoom().setLights(true);
    }


}
