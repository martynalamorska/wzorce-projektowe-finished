package command;

import java.io.IOException;

public class PlayTrackCommand extends Command {

    private String track;

    public PlayTrackCommand(Room room, String track) {
        super(room);
        this.track = track;
    }

    @Override
    public void execute() throws IOException {
        getRoom().setMusic(track);
    }
}
