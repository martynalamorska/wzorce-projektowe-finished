package singleton;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {

    private Logger() {

    }

    private static Logger a = null;

    public static Logger getLogger() {
        if (a == null) {
            return new Logger();
        } else {
            return a;
        }
    }

    public void log(String x) throws IOException {

        FileWriter fileWriter = new FileWriter("asd.txt", true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.append(x + "\n");
        bufferedWriter.close();
        fileWriter.close();


    }

}
