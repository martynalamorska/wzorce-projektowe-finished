package chainofresponsibility;

import java.util.LinkedList;
import java.util.List;

public class ATM1Bill extends Chain{

    public ATM1Bill() {

    }

    public List<Bill> withdraw(int amount) {
        List<Bill> billList = new LinkedList<Bill>();
        int billAmount = amount;
        for (int i = 0; i < billAmount; i++) {
            billList.add(Bill.build1DollarBill());
        }
        return billList;
    }
}
