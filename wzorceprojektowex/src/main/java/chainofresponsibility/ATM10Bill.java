package chainofresponsibility;

import java.util.LinkedList;
import java.util.List;

public class ATM10Bill extends Chain{

    public ATM10Bill(Chain next) {
        super(next);
    }

    public List<Bill> withdraw(int amount) {
        List<Bill> billList = new LinkedList<Bill>();
        int billAmount = amount / 10;
        int remaining = amount % 10;
        for (int i = 0; i < billAmount; i++) {
            billList.add(Bill.build10DollarBill());
        }
        if (next != null && remaining != 0) {
            billList.addAll(next.withdraw(remaining));
        }
        return billList;
    }
}
