package chainofresponsibility;

import java.util.LinkedList;
import java.util.List;

public class ATM50Bill extends Chain {

    public ATM50Bill(Chain next) {
        super(next);
    }

    public List<Bill> withdraw(int amount) {
        List<Bill> billList = new LinkedList<Bill>();
        int billAmount = amount / 50;
        int remaining = amount % 50;
        for (int i = 0; i < billAmount; i++) {
            billList.add(Bill.build50DollarBill());
        }
        if (next != null && remaining != 0) {
            billList.addAll(next.withdraw(remaining));
        }
        return billList;
    }


}
