package chainofresponsibility;

import java.util.List;

public abstract class Chain {

    public Chain next;

    public Chain(){
    }


    public Chain(Chain next) {
        this.next = next;
    }

    public abstract List<Bill> withdraw(int amount);

}
