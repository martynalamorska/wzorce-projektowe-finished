package chainofresponsibility;

import java.util.LinkedList;
import java.util.List;

public class ATM20Bill extends Chain{

    public ATM20Bill(Chain next) {
        super(next);
    }

    public List<Bill> withdraw(int amount) {
        List<Bill> billList = new LinkedList<Bill>();
        int billAmount = amount / 20;
        int remaining = amount % 20;
        for (int i = 0; i < billAmount; i++) {
            billList.add(Bill.build20DollarBill());
        }
        if (next != null && remaining != 0) {
            billList.addAll(next.withdraw(remaining));
        }
        return billList;
    }
}
