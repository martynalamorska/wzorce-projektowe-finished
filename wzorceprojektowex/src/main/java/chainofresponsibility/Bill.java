package chainofresponsibility;

public class Bill {

    private int value;

    private Bill(final int value){
        this.value = value;
    }

    public static Bill build1DollarBill() {
        return new Bill(1);
    }

    public static Bill build10DollarBill() {
        return new Bill(10);
    }

    public static Bill build20DollarBill() {
        return new Bill(20);
    }

    public static Bill build50DollarBill() {
        return new Bill(50);
    }

    @Override
    public String toString() {
        return value + "bill";
    }
}
