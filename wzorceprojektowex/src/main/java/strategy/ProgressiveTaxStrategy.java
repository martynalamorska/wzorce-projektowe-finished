package strategy;

import java.math.BigDecimal;

public class ProgressiveTaxStrategy implements TaxStrategy {

    private BigDecimal taxThreshold = BigDecimal.valueOf(85528);
    private BigDecimal lowerPercentage = BigDecimal.valueOf(0.18);
    private BigDecimal higherPercentage = BigDecimal.valueOf(0.32);

    @Override
    public BigDecimal calculateTax(BigDecimal amount) {
        if (amount.compareTo(taxThreshold) < 0) {
            return amount.multiply(lowerPercentage)
                    .subtract(BigDecimal.valueOf(556.02));
        } else {
            return (taxThreshold.multiply(lowerPercentage))
                    .add((amount.subtract(taxThreshold))
                    .multiply(higherPercentage));
        }
    }
}

