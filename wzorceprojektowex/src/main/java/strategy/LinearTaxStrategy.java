package strategy;

import java.math.BigDecimal;

public class LinearTaxStrategy implements TaxStrategy {

    @Override
    public BigDecimal calculateTax(BigDecimal amount) {
        return amount.multiply(BigDecimal.valueOf(0.19));
    }

}
