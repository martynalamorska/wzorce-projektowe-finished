package facade;

public class Main {

    public static void main(String[] args) {

        CourseRepository courseRepository = new CourseRepository();
        CourseFacade courseFacade = new CourseFacade(courseRepository);

        courseRepository.addToList("Martyna Lamorska");
        courseRepository.addToList("Alan Turek");
        courseRepository.addToList("Łukasz Wróbel");
        courseRepository.addToList("Jakub Hepo");
        courseRepository.addToList("Monika Piotrowska");
        courseRepository.addToList("Kasia Urbaniec");
        courseRepository.addToList("Mateusz Tosta");
        courseRepository.addToList("Krzysztof Bac");
        courseRepository.addToList("Łukasz Leśniewicz");

        courseFacade.getUsersByLength(10);
        System.out.println();
        courseFacade.getUsersInAlphabeticalOrder().forEach(System.out::println);
        System.out.println();
        courseFacade.getUsersStartingWith("ma");
    }
}
