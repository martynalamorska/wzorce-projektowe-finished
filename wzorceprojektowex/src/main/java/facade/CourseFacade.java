package facade;

import java.util.List;
import java.util.stream.Collectors;

public class CourseFacade {

    private CourseRepository cR;

    public CourseFacade(CourseRepository cR) {
        this.cR = cR;
    }

    public List<String> getUsersInAlphabeticalOrder(){
        return cR.getList().stream().sorted().collect(Collectors.toList());
    }

    public void getUsersStartingWith(String x){
        cR.getList().stream()
                .filter(a -> a.toLowerCase().startsWith(x))
                .forEach(System.out::println);
    }

    public void getUsersByLength(int x){
        cR.getList().stream()
                .filter(a -> a.length()==x)
                .forEach(System.out::println);
    }

//    public void getUserWithLongestName(){
//        cR.getList().stream()
//                .max(Comparator.comparingInt(String::length).reversed())
//                .
//    }

}
