package facade;

import java.util.LinkedList;
import java.util.List;

public class CourseRepository {

    List<String> courseList = new LinkedList<>();

    public List<String> getList(){
        return courseList;
    }

    public void addToList(String a){
        courseList.add(a);
    }



//    public String getUsersByLetters(String x){
//        for (int i = 0; i < courseList.size(); i++) {
//            if (courseList.get(i).startsWith(x)){
//                return courseList.get(i);
//            }
//        }
//        return null;
//    }
//
//    public String getUsersByNameLength(int a){
//        for (int i = 0; i < courseList.size(); i++) {
//            if(courseList.get(i).length() == a){
//                return courseList.get(i);
//            }
//        }
//        return null;
//    }

}
