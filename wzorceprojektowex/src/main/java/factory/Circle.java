package factory;

public class Circle implements Shape {

    private double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    public double circumference() {
        return 2*Math.PI*r;
    }

    @Override
    public double area() {
        return Math.pow(Math.PI*r, 2);
    }
}
