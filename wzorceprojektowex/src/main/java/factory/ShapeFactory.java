package factory;

public class ShapeFactory {

    public static Shape buildShape(String type, double x) {
        if (type.equals("circle")) {
            return new Circle(x);
        } else if (type.equals("square")) {
            return new Square(x);
        } else {
            return null;
        }
    }
}
