package factory;

public class Square implements Shape {

    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double circumference() {
        return a*4;
    }

    @Override
    public double area() {
        return Math.pow(a, 2);
    }
}
