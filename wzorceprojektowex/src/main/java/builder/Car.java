package builder;


import singleton.Logger;

import java.io.IOException;

public class Car {

    private double engine;
    private String chassis;
    private String interior;
    private String body;

    private Car(final CarBuilder carBuilder) {
        this.engine = carBuilder.engine;
        this.chassis = carBuilder.chassis;
        this.interior = carBuilder.interior;
        this.body = carBuilder.body;
    }

    public double getEngine() {
        return engine;
    }

    public String getChassis() {
        return chassis;
    }

    public String getInterior() {
        return interior;
    }

    public String getBody() {
        return body;
    }

    public static class CarBuilder {
        @Override
        public String toString() {
            return "CarBuilder{" +
                    "engine='" + engine + '\'' +
                    ", chassis='" + chassis + '\'' +
                    ", interior='" + interior + '\'' +
                    ", body='" + body + '\'' +
                    '}';
        }

        private double engine;
        private String chassis;
        private String interior;
        private String body;

        public CarBuilder setEngine(double engine) throws IOException {
            this.engine = engine;
            Logger.getLogger().log(String.format("engine built with %s", engine));
            return this;
        }

        public CarBuilder setChassis(final String chassis) throws IOException {
            this.chassis = chassis;
            Logger.getLogger().log(String.format("chassis built with %s", chassis));
            return this;
        }

        public CarBuilder setInterior(String interior) throws IOException {
            this.interior = interior;
            Logger.getLogger().log(String.format("interior built with %s", interior));
            return this;
        }

        public CarBuilder setBody(String body) throws IOException {
            this.body = body;
            Logger.getLogger().log(String.format("body built with %s", body));
            return this;

        }

        public Car build() {
            return new Car(this);
        }


    }
}
