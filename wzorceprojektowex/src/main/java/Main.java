
import builder.Car;
import chainofresponsibility.ATM10Bill;
import chainofresponsibility.ATM1Bill;
import chainofresponsibility.ATM20Bill;
import chainofresponsibility.ATM50Bill;
import command.*;
import facade.CourseFacade;
import facade.CourseRepository;
import factory.Shape;
import factory.ShapeFactory;
import singleton.Logger;
import strategy.LinearTaxStrategy;
import strategy.ProgressiveTaxStrategy;

import java.io.IOException;
import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) throws IOException {

        //factory
        Shape circle = ShapeFactory.buildShape("circle", 2);
        Shape square = ShapeFactory.buildShape("square", 5);

        if (circle != null) {
            System.out.println("Circle's area: " + circle.area() + ", circle's circumference: " + circle.circumference());
        }
        if (square != null) {
            System.out.println("Square's area: " + square.area() + ", square's circumference: " + square.circumference());
        }

        //facade
        CourseRepository courseRepository = new CourseRepository();
        CourseFacade courseFacade = new CourseFacade(courseRepository);

        courseRepository.addToList("Martyna Lamorska");
        courseRepository.addToList("Alan Turek");
        courseRepository.addToList("Łukasz Wróbel");
        courseRepository.addToList("Jakub Hepo");
        courseRepository.addToList("Monika Piotrowska");
        courseRepository.addToList("Kasia Urbaniec");
        courseRepository.addToList("Mateusz Tosta");
        courseRepository.addToList("Krzysztof Bac");
        courseRepository.addToList("Łukasz Leśniewicz");

        courseFacade.getUsersByLength(10);
        System.out.println();
        courseFacade.getUsersInAlphabeticalOrder().forEach(System.out::println);
        System.out.println();
        courseFacade.getUsersStartingWith("ma");

        //singleton
        Logger logger = Logger.getLogger();
        logger.log("qwerty");

        //builder
        new Car.CarBuilder().setBody("body")
                .setChassis("chassis")
                .setEngine(1.7)
                .setInterior("interior")
                .build();

        //chain of responsibility
        new ATM50Bill(new ATM20Bill(new ATM10Bill(new ATM1Bill()))).withdraw(379).forEach(System.out::println);

        //command
        Room room1 = new Room("Room1");
        Room room2 = new Room("Room2");
        Room room3 = new Room("Room3");
        Executor executor = new Executor();
        executor.executeCommand(new LightsOnCommand(room1));
        executor.executeCommand(new LightsOnCommand(room3));
        executor.executeCommand(new PlayTrackCommand(room2, "ABC"));
        executor.executeCommand(new LightsOffCommand(room3));

        System.out.println(room2);
        logger.log(room2.toString());

        //strategy
        ProgressiveTaxStrategy progTax = new ProgressiveTaxStrategy();
        LinearTaxStrategy linTax = new LinearTaxStrategy();

        System.out.println(linTax.calculateTax(BigDecimal.valueOf(63748)));
        System.out.println(progTax.calculateTax(BigDecimal.valueOf(123456)));
        System.out.println(progTax.calculateTax(BigDecimal.valueOf(23870)));

    }
}
